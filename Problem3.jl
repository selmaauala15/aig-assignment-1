### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ 37493479-91e4-4527-99ee-526b2ed06524
using Pkg

# ╔═╡ 55957650-1d4f-486f-8062-41c1cc202645
Pkg.activate("Project.toml")

# ╔═╡ f9e8daab-c6d9-42f1-ada7-99bc20e181fb
Pkg.add("Formatting"); using Formatting

# ╔═╡ ad7b3e6b-a5f3-4a21-bb35-14e819ba025b
Pkg.add("JSON"); using JSON

# ╔═╡ c11c6cb1-d293-47c3-83b1-894c5a06775a
Pkg.add("LightGraphs"); using LightGraphs

# ╔═╡ 450d3b82-610d-40dc-b704-8d14b5ba0910
Pkg.add("MathOptInterface"); using MathOptInterface

# ╔═╡ a63ae05b-e09c-404c-8cd7-fcae4b6c1672
Pkg.add("MatrixNetworks"); using MatrixNetworks

# ╔═╡ 23310f33-1481-455a-81e6-640d4e14d6bb
Pkg.add("StatsBase"); using StatsBase

# ╔═╡ a11f50ed-f652-41f5-9a3d-75d3424bc151
Pkg.add("StatsFuns"); using StatsFuns

# ╔═╡ b6f670e8-8ab9-4161-bd9c-ce44733bc337
Pkg.add("JuMP")

# ╔═╡ e79ad0ec-d31a-4ce7-8109-688a2ef97a63
Pkg.add("ConstraintSolver")

# ╔═╡ d0f165cc-9cbd-4d1e-ad21-a907c1b44817
using DataStructures

# ╔═╡ 91f9c12f-7b23-4df3-adb1-ae1244c29254
using JuMP:
    @variable,
    @constraint,
    @objective,
    Model,
    optimizer_with_attributes,
    VariableRef,
    backend,
    set_optimizer,
    direct_model,
    optimize!,
    objective_value,
    set_lower_bound,
    set_upper_bound,
    termination_status

# ╔═╡ 4d4514e7-11bb-4bf2-9111-c8fef61df118
using Random

# ╔═╡ db3bd83f-79d2-43de-bad7-6395ce835965
using Statistics

# ╔═╡ 33258bba-96e0-4306-a427-df2126c7b750
using PlutoUI

# ╔═╡ bf9f59fb-4108-4417-afe7-e312adb8aeea
import JuMP.sense_to_set

# ╔═╡ c903d38c-3c98-4521-b57b-bdf3d5b51a47


# ╔═╡ 2959439d-05e0-47bb-a212-799e462b13cd


# ╔═╡ 39be09e0-23f8-4d2e-8603-de73a63e39f8
begin
	push!(com.constraints, (func, indices))
	current_constraint_number = length(com.constraints)
	for i in indices
	    # only if index is in search space
	    if haskey(com.subscription, i)
	        push!(com.subscription[i], current_constraint_number)
	    end
	end
end

# ╔═╡ 77fdc49e-8a94-4fca-9752-ce17ee28badb
function solve(com::CS.CoM)
    return :NotSolved
end

# ╔═╡ ec793dce-cb05-4e7f-ba59-733c32cd1fb6
function solve(com::CS.CoM)
    if length(com.search_space) == 0
        return :Solved
    end
    feasible = true
    func_calls = 0

    for constraint in com.constraints
        funcname, indices = constraint
        if findfirst(v->v == com.not_val, com.grid[indices]) === nothing 
            continue
        end
        feasible = funcname(com, indices)
        func_calls += 1
        if !feasible
            break
        end
    end
    if !feasible
        return :Infeasible
    end

    println("#func calls: ",func_calls)
    if length(com.search_space) == 0
        return :Solved
    end
    num_open = 0
    for ssi in com.search_space
        num_open += length(keys(ssi.second))
    end
    println("Size of search space: ", num_open)
    return :NotSolved

# ╔═╡ 4740d55c-ed39-475b-afaf-49ce1dcbdf14
const CS = ConstraintSolver

# ╔═╡ a77e45ed-9a5d-40d2-acfc-7ea154473680
begin
	using JuMP
	
	grid = []
	
	using ConstraintSolver
	
	# creating a constraint solver model and setting ConstraintSolver as the optimizer.
	m = Model(CS.Optimizer) 
	# define the 81 variables
	@variable(m, X2 <= x[1:9,1:9] <= 9, Int)
	# set variables if fixed
	for r=1:9, c=1:9
	    if grid[r,c] != 0
	        @constraint(m, x[r,c] == grid[r,c])
	    end
	end
	
	for rc = 1:9
	    @constraint(m, X2[rc,:] in CS.AllDifferentSet())
	    @constraint(m, X2[:,rc] in CS.AllDifferentSet())
	end
	
	for br=0:2
	    for bc=0:2
	        @constraint(m, vec(x[br*3+1:(br+1)*3,bc*3+1:(bc+1)*3]) in CS.AllDifferentSet())
	    end
	end
	
	optimize!(m)
	
	# retrieve grid
	grid = convert.(Int, JuMP.value.(x))
end

# ╔═╡ Cell order:
# ╠═37493479-91e4-4527-99ee-526b2ed06524
# ╠═55957650-1d4f-486f-8062-41c1cc202645
# ╠═d0f165cc-9cbd-4d1e-ad21-a907c1b44817
# ╠═f9e8daab-c6d9-42f1-ada7-99bc20e181fb
# ╠═ad7b3e6b-a5f3-4a21-bb35-14e819ba025b
# ╠═91f9c12f-7b23-4df3-adb1-ae1244c29254
# ╠═bf9f59fb-4108-4417-afe7-e312adb8aeea
# ╠═c11c6cb1-d293-47c3-83b1-894c5a06775a
# ╠═450d3b82-610d-40dc-b704-8d14b5ba0910
# ╠═a63ae05b-e09c-404c-8cd7-fcae4b6c1672
# ╠═4d4514e7-11bb-4bf2-9111-c8fef61df118
# ╠═db3bd83f-79d2-43de-bad7-6395ce835965
# ╠═23310f33-1481-455a-81e6-640d4e14d6bb
# ╠═a11f50ed-f652-41f5-9a3d-75d3424bc151
# ╠═b6f670e8-8ab9-4161-bd9c-ce44733bc337
# ╠═33258bba-96e0-4306-a427-df2126c7b750
# ╠═e79ad0ec-d31a-4ce7-8109-688a2ef97a63
# ╠═4740d55c-ed39-475b-afaf-49ce1dcbdf14
# ╠═c903d38c-3c98-4521-b57b-bdf3d5b51a47
# ╠═a77e45ed-9a5d-40d2-acfc-7ea154473680
# ╠═2959439d-05e0-47bb-a212-799e462b13cd
# ╠═39be09e0-23f8-4d2e-8603-de73a63e39f8
# ╠═77fdc49e-8a94-4fca-9752-ce17ee28badb
# ╠═ec793dce-cb05-4e7f-ba59-733c32cd1fb6

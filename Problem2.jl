### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ d96a1d1d-3e06-4d3f-9d1f-481968514a1c
using Pkg

# ╔═╡ 1d42d0a3-1902-4e9e-bd71-ff1d01379c5f
Pkg.activate("Project.toml")

# ╔═╡ 68cf2fc5-6a1e-48b1-973b-c6c07be2142a
using PlutoUI

# ╔═╡ 1ecd4fea-ef7d-487d-ab6a-5982ff77b514
maxi=1000

# ╔═╡ 18de0e52-0f8d-40f9-9044-e6e8d8a036b2
mini=-1000

# ╔═╡ ef05d25c-e8be-41f7-9942-fef8806ecbe6
function minimax(Depth,Game,Alpha,Beta,Player,Index)
	if Depth==3
		return Game[Index]
	end
	
	if Player
		
		a = mini
		for i in 1:2
			value = minimax(Depth - 1,i,Alpha,Beta,false,Game)
			a = a >= value ? a : value
			if Beta <= Alpha
				break
			end
		end
		return a
	else 
		a = maxi
		for i in 1:2
			value = minimax(d -1,i,Alpha,Beta,true,Game)
			a = a <= value ? a : value
			Beta = Beta <= value ? beta : value
			if Beta <= Alpha
				break
			end
		end
		return a
	end
end

# ╔═╡ 5d9b2b59-8643-4511-adc5-1ab6ea3063fd
GM = [3,5,10,2,8,19,2,7,3]

# ╔═╡ 25887b3f-8314-4eef-b73a-633416b33dd8
minimax(3,GM,maxi,mini,true,1)

# ╔═╡ Cell order:
# ╠═d96a1d1d-3e06-4d3f-9d1f-481968514a1c
# ╠═1d42d0a3-1902-4e9e-bd71-ff1d01379c5f
# ╠═68cf2fc5-6a1e-48b1-973b-c6c07be2142a
# ╠═1ecd4fea-ef7d-487d-ab6a-5982ff77b514
# ╠═18de0e52-0f8d-40f9-9044-e6e8d8a036b2
# ╠═ef05d25c-e8be-41f7-9942-fef8806ecbe6
# ╠═5d9b2b59-8643-4511-adc5-1ab6ea3063fd
# ╠═25887b3f-8314-4eef-b73a-633416b33dd8

### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ 7af53bf0-ae54-11eb-03f6-adf07c9322ff
using Pkg

# ╔═╡ 67ed67d9-fee6-473b-bb59-2c91e1df00aa
Pkg.activate("Project.toml")

# ╔═╡ f412ffcd-7a17-4fa4-bb5c-b5660bf1394f
Pkg.add("DataStructures")

# ╔═╡ 550411b9-8aa6-4984-b528-061f9269666e
using PlutoUI

# ╔═╡ e5d788c0-3cff-4a7c-8029-bc8bf71390ce
using DataStructures

# ╔═╡ 252f58b7-84cd-4634-90b4-0566af024a97
mutable struct Action 
    Name::String
    Cost::Int64
end

# ╔═╡ f9d07e8e-a45b-4d0f-92cc-f7b39ef2a783
RE = Action("remain", 1)

# ╔═╡ c87ea3d2-16aa-4e51-bb12-6eb1a72dc542
ME = Action("move east", 3)

# ╔═╡ d7d52180-fcb0-4bc4-bd38-a85df5ebe45a
MW = Action("move west", 3)

# ╔═╡ 9df78458-3d09-4859-8b19-e5b15a361dda
CO = Action("collect item", 5)

# ╔═╡ a27e4377-61fa-41c1-8949-9dad2b6f408c
mutable struct State
	Name::String
	hasDirt::Vector{Int64}
	Position::Int64
end

# ╔═╡ 552bce37-80fc-4e03-bc32-dd2a9f41aa74
Zero = State("State Zero", [1, 3, 2,1], 2)

# ╔═╡ 257d9b8a-4087-4077-8788-75d4592f4eba
One= State("State One",[1,2,2,1],2)

# ╔═╡ 19421068-2cee-45c6-8b1e-eb09d5a92cad
Two= State("State Two",[1,1,2,1],1)

# ╔═╡ 24155c6f-6c91-4d1b-a9b7-7d92ba3b9332
Three= State("State Three",[0,1,2,1],2)

# ╔═╡ 13a31eb1-025b-4305-adf1-75f3f7adeceb
Four= State("State Four",[0,0,2,1],3)

# ╔═╡ f94f0222-1b5a-4e81-95d7-1a35e8c0045d
Five= State("State Five",[0,0,1,1],3)

# ╔═╡ cf40dc08-b049-4450-ace6-84c0be615675
Six= State("State Six",[0,0,0,1],4)

# ╔═╡ e9e1f8b2-aae8-49e3-9a33-cfc73745bf96
Seven= State("State Seven",[0,0,0,0],4)

# ╔═╡ 55abeaba-90f5-4a1e-8649-34f6f88f49b5
TransitionModel= Dict()

# ╔═╡ e248b1bd-bbae-4aa6-8a9c-e30850657ef4
push!(TransitionModel,Zero => [(CO,One),(RE, One)])

# ╔═╡ 4f6f0869-6dbf-4e74-a48f-2b0a72f814a1
push!(TransitionModel,One=>[(CO,Two),(MW,Two)])

# ╔═╡ a149f821-c50d-4e03-a373-efbafa0421b5
push!(TransitionModel,Two=>[(CO,Three),(ME,Three)])

# ╔═╡ 6198957a-4a07-4e0e-8775-a61e4172be02
push!(TransitionModel,Three=>[(CO,Four),(ME,Four)])

# ╔═╡ 0ac007f4-2745-41c2-a1bd-6250d929b06d
push!(TransitionModel,Four=>[(CO,Five),(RE,Five)])

# ╔═╡ 96db0f34-c431-4a3e-8120-7446f1c89710
push!(TransitionModel,Five=>[(CO,Six),(ME,Six)])

# ╔═╡ afdd0cf7-8eaf-4254-8c66-fb164a476b08
push!(TransitionModel,Six=>[(CO,Seven),(RE,Seven)])

# ╔═╡ fb6f4251-1030-4e87-a30e-44a2c2ba7924
TransitionModel

# ╔═╡ 98efe1e4-ee4f-4971-8cfd-733d32ccb24d
Goalstate=[Seven]

# ╔═╡ e4282fc7-e3bb-467e-b7e9-716ad8eec171
function create_result(trans_model, ancestors, initial_state, goal_state)

	result = []
	explorer = goal_state
	while !(explorer == initial_state)
		current_state_ancestor = ancestors[explorer]
		related_transitions = trans_model[current_state_ancestor]
		for single_trans in related_transitions
			if single_trans[2] == explorer
				push!(result, single_trans[1])
				break
			else
				continue
			end
		end
		explorer = current_state_ancestor
	end
	return result
end

# ╔═╡ 12bab5dc-88a4-4b07-9df5-31c1c3a1e950
function astar(start, goal)
	start = time()
	pr_queue = []
	
	visited = Set()
	while length(pr_queue) !=0
		_, cost, path, current = heappop!(pr_queue)
		if current == goal
			return path, push!(visited, current)
		end
		if current in visited
			continue
		end
		push!(visited, current)
		
	end
	
end

# ╔═╡ b6d14ba5-51e7-40e9-ad02-690c50f2d8a5
function heuristic(cell,goal)
	return astar
end

# ╔═╡ 164b2d23-7714-42d5-a2d5-f337f83eb81f
astar(one, Seven)

# ╔═╡ Cell order:
# ╠═7af53bf0-ae54-11eb-03f6-adf07c9322ff
# ╠═67ed67d9-fee6-473b-bb59-2c91e1df00aa
# ╠═550411b9-8aa6-4984-b528-061f9269666e
# ╠═f412ffcd-7a17-4fa4-bb5c-b5660bf1394f
# ╠═e5d788c0-3cff-4a7c-8029-bc8bf71390ce
# ╠═252f58b7-84cd-4634-90b4-0566af024a97
# ╠═f9d07e8e-a45b-4d0f-92cc-f7b39ef2a783
# ╠═c87ea3d2-16aa-4e51-bb12-6eb1a72dc542
# ╠═d7d52180-fcb0-4bc4-bd38-a85df5ebe45a
# ╠═9df78458-3d09-4859-8b19-e5b15a361dda
# ╠═a27e4377-61fa-41c1-8949-9dad2b6f408c
# ╠═552bce37-80fc-4e03-bc32-dd2a9f41aa74
# ╠═257d9b8a-4087-4077-8788-75d4592f4eba
# ╠═19421068-2cee-45c6-8b1e-eb09d5a92cad
# ╠═24155c6f-6c91-4d1b-a9b7-7d92ba3b9332
# ╠═13a31eb1-025b-4305-adf1-75f3f7adeceb
# ╠═f94f0222-1b5a-4e81-95d7-1a35e8c0045d
# ╠═cf40dc08-b049-4450-ace6-84c0be615675
# ╠═e9e1f8b2-aae8-49e3-9a33-cfc73745bf96
# ╠═55abeaba-90f5-4a1e-8649-34f6f88f49b5
# ╠═e248b1bd-bbae-4aa6-8a9c-e30850657ef4
# ╠═4f6f0869-6dbf-4e74-a48f-2b0a72f814a1
# ╠═a149f821-c50d-4e03-a373-efbafa0421b5
# ╠═6198957a-4a07-4e0e-8775-a61e4172be02
# ╠═0ac007f4-2745-41c2-a1bd-6250d929b06d
# ╠═96db0f34-c431-4a3e-8120-7446f1c89710
# ╠═afdd0cf7-8eaf-4254-8c66-fb164a476b08
# ╠═fb6f4251-1030-4e87-a30e-44a2c2ba7924
# ╠═98efe1e4-ee4f-4971-8cfd-733d32ccb24d
# ╠═e4282fc7-e3bb-467e-b7e9-716ad8eec171
# ╠═b6d14ba5-51e7-40e9-ad02-690c50f2d8a5
# ╠═12bab5dc-88a4-4b07-9df5-31c1c3a1e950
# ╠═164b2d23-7714-42d5-a2d5-f337f83eb81f
